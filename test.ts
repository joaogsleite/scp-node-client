import SCP from './'

async function test () {
  const client = new SCP({
    host: '...',
    user: 'root',
  })
  try {
    await client.send('./example.txt')
    await client.get('./example.txt')
  } catch(error) {
    console.error(error)
  }
}

test()
