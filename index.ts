import * as scp from 'scp'

interface SCPConfig {
  host: string,
  port?: number,
  user: string,
  path?: string,
}

export default class SCP {

  config: SCPConfig

  constructor (config: SCPConfig) {
    this.config = {
      ...config,
      path: config.path
        ? config.path
        : '~'
    }
  }

  send (file: string, remotePath?: string) {
    return new Promise((resolve, reject) => {
      const options = {
        ...this.config,
        file,
      }
      if (remotePath) {
        options.path = remotePath
      }
      scp.send(options, (error) => {
        if (error) {
          reject(error)
        } else {
          resolve()
        }
      })
    })
  }
  get (file: string, localPath?: string) {
    return new Promise((resolve, reject) => {
      const options = {
        ...this.config,
        file,
      }

      if (file.startsWith('/')) {
        options.file = file
      } else {
        options.file = `${this.config.path}/${file}`
      }

      if (localPath) {
        options.path = localPath
      } else {
        options.path = './'
      }

      scp.get(options, (error) => {
        if (error) {
          reject(error)
        } else {
          resolve()
        }
      })
    })
  }
}
